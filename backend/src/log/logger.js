const path = require('path');
const dateFormat = require('dateformat');

const now = new Date();

const pathLog = path.join(__dirname, "/files", dateFormat(now, "yyyy_mmm_dd_(HH-MM-ss-l)") + ".log");

logger = require('simple-node-logger').createSimpleLogger({
    logFilePath: pathLog,
    timestampFormat: 'YYYY-MM-DD HH:mm:ss.SSS'
});

const log_error_sql = function (err) {
    logger.log("error", "Error: "
        + "\n\tcode: " + err.code
        + "\n\tfatal: " + err.fatal
        + "\n\tsql: " + err.sql
        + "\n\tsqlMessage: " + err.sqlMessage
        + "\n\tstack: " + err.stack);
}

const log_plain_info = function (message) {
    logger.log("info", message);
}

const log_any_error = function (err) {
    logger.log("error", "Error: "
        + "\n\tstack: " + err.stack);
}

module.exports.l = logger;
module.exports.lsql = log_error_sql;
module.exports.info = log_plain_info;
module.exports.e = log_any_error;