"use strict";

const path = require('path');
const express = require('express');
const router = express.Router();
module.exports = router;
const mysql = require("mysql");
const dateformat = require('dateformat');

const db = require(path.join(__dirname, "/../../database/database.js"));
const q = require(path.join(__dirname, "/../../database/query.js"));
const l = require(path.join(__dirname, "/../../log/logger.js"));

const disk_table = db.table.catalogue_disks;
const tires_table = db.table.catalogue_tires;
const orders_table = db.table.orders_list;

const disk_table_name = db.db.db_name + "." + disk_table.table_name;
const tires_table_name = db.db.db_name + "." + tires_table.table_name;
const orders_table_name = db.db.db_name + "." + orders_table.table_name;

const service_mail_from_user = "PitStopMDA97779@gmail.com";
const service_mail_from_pass = "97779mda";

const nodemailer = require('nodemailer');

var mail_transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: service_mail_from_user,
        pass: service_mail_from_pass
    }
});

router.post('/basket/submit', function (req, res) {
    if (req.session && req.session.basket) {
        if (req.query
            && req.query.email && req.query.name && req.query.phone
            && req.query.shipping_address_country && req.query.shipping_type
            && req.query.shipping_address_department && req.query.payment_type
            && req.query.order_positions && req.query.total_sum) {

            let con = mysql.createConnection(db.customer_con_opts);
            con.connect(function (err_con) {
                if (err_con) {
                    l.lsql(err_con);
                }

                let shipping_type;
                if (req.query.shipping_type == "novapost") {
                    shipping_type = "Новая Почта";
                } else if (req.query.shipping_type == "intime") {
                    shipping_type = "ИнТайм";
                } else if (req.query.shipping_type == "address") {
                    shipping_type = "Адресная доставка";
                }

                let payment_type;
                if (req.query.payment_type == "card") {
                    payment_type = "банковской картой";
                } else if (req.query.payment_type == "nalplat") {
                    payment_type = "наложенный платеж";
                }

                let order_positions;
                try {
                    order_positions = JSON.parse(req.query.order_positions);
                } catch (e) {
                    l.e(e);
                    res.send("error");
                    return;
                }
                con.query(q.fun_insert_simple(
                    orders_table_name,
                    [orders_table.cols.date_sent.colname,
                    orders_table.cols.customer_name.colname,
                    orders_table.cols.customer_email.colname,
                    orders_table.cols.customer_telephone.colname,
                    orders_table.cols.shipping_address.colname,
                    orders_table.cols.shipping_type.colname,
                    orders_table.cols.payment_type.colname,
                    orders_table.cols.order_positions.colname,
                    orders_table.cols.order_status.colname,
                    orders_table.cols.total_sum.colname],

                    [con.escape(new Date()),
                    con.escape(req.query.name),
                    con.escape(req.query.email),
                    con.escape(req.query.phone),
                    con.escape(req.query.shipping_address_country + " | " + req.query.shipping_address_department),
                    con.escape(req.query.shipping_type),
                    con.escape(req.query.payment_type),
                    con.escape(req.query.order_positions),
                    con.escape('pending'),
                    con.escape(req.query.total_sum)
                    ]), function (err_ins) {
                        if (err_ins) {
                            l.lsql(err_ins);
                        }

                        let ins_id;
                        con.query("SELECT LAST_INSERT_ID() AS idOrder;", function (err_sel, res_sel) {
                            if (err_sel) {
                                l.lsql(err_sel);
                            }

                            if (res_sel && res_sel[0]) {
                                ins_id = res_sel[0].idOrder;
                            }

                            con.end(function (err_end) {
                                if (err_end) {
                                    l.lsql(err_end);
                                }

                                let message_html = '<p> Спасибо, Ваш заказ #' + ins_id + ' отправлен на обработку! Оператор свяжется с вами в ближайшее время.</p>'
                                    + '<br>'
                                    + '<p><strong>Детали заказа:</strong></p>'
                                    + '<p>Номер заказа: ' + ins_id
                                    + '<p>Имя покупателя: ' + req.query.name + '</p>'
                                    + '<p>Телефон: ' + req.query.phone + '</p>'
                                    + '<p>Адрес доставки: ' + req.query.shipping_address_country + " | " + req.query.shipping_address_department + '</p>'
                                    + '<p>Способ доставки: ' + shipping_type + '</p>'
                                    + '<p>Способ оплаты: ' + payment_type + '</p>'
                                    + '<br>'
                                    + '<p><strong>Позиции в заказе (' + order_positions.length + '):</strong></p>';

                                let item_type;
                                for (let i = 0; i < order_positions.length; ++i) {
                                    item_type = (order_positions[i].type == "tires" ? "шины" : "диски");

                                    message_html += '<p>' + (i + 1) + '. ' + order_positions[i].name + '  —  '
                                        + order_positions[i].amount + ' шт | '
                                        + order_positions[i].amount + ' * ' + order_positions[i].price_moment
                                        + ' = ' + (parseInt(order_positions[i].amount, 10) * parseInt(order_positions[i].price_moment)) + ' грн'
                                        + ' (' + item_type + ')'
                                        + '</p>';
                                }

                                message_html += "<p><strong>К оплате: " + req.query.total_sum + " грн</strong>";

                                let mail_options = {
                                    from: service_mail_from_user,
                                    to: req.query.email,
                                    subject: 'PitStop Заказ #' + ins_id,
                                    html: message_html
                                };

                                mail_transporter.sendMail(mail_options, function (error, info) {
                                    if (error) {
                                        l.e(error);
                                    } else {
                                        l.info('Email sent: ' + info.response);
                                    }
                                });

                                req.session.basket = [];

                                res.send("submitted");
                            });
                        });
                    });
            });
        } else {
            res.send("error");
        }
    } else {
        res.send("error");
    }
});

router.post('/basket/change', function (req, res) {
    if (req.query && req.query.id && req.query.type && req.session) {

        let pos = JSON.stringify({ type: req.query.type, id: req.query.id });

        if (!req.session.basket) {
            req.session.basket = [];
        }

        if (req.session.basket.includes(pos)) {
            req.session.basket.splice(req.session.basket.indexOf(pos), 1);
            res.send("removed");
        } else {
            req.session.basket.push(pos);
            res.send("added");
        }
    } else { res.send("error"); }
});

router.get('/basket', function (req, res) {
    let render_params = {};

    if (req.session && req.session.basket) {
        render_params.basket_amount = req.session.basket.length;

        // Parse ids and query elements
        let con = mysql.createConnection(db.customer_con_opts);
        con.connect(function (err_con) {
            if (err_con) {
                l.lsql(err_con);
            }

            let items_tires = [], items_disks = [], total_sum = 0;
            let parsed_el;

            for (let i = 0; i < req.session.basket.length; ++i) {
                parsed_el = JSON.parse(req.session.basket[i]);

                if (parsed_el.type == "disks") {
                    con.query(q.fun_select_simple(
                        disk_table_name,
                        ["*"],
                        "idProduct=" + con.escape(parsed_el.id),
                        null,
                        1
                    ), function (sel_err, sel_res) {
                        if (sel_err) {
                            l.lsql(sel_err);
                        }

                        if (sel_res && sel_res[0]) {
                            items_disks.push(sel_res[0]);
                            total_sum += Math.ceil(sel_res[0].end_rozn_price);
                        }
                    });
                } else if (parsed_el.type == "tires") {
                    con.query(q.fun_select_simple(
                        tires_table_name,
                        ["*"],
                        "idProduct=" + con.escape(parsed_el.id),
                        null,
                        1
                    ), function (sel_err, sel_res) {
                        if (sel_err) {
                            l.lsql(sel_err);
                        }

                        if (sel_res && sel_res[0]) {
                            items_tires.push(sel_res[0]);
                            total_sum += Math.ceil(sel_res[0].end_rozn_price);
                        }
                    });
                }
            }

            con.end(function (con_end_err) {
                if (con_end_err) {
                    l.lsql(con_end_err);
                }

                render_params.items_disks = items_disks;
                render_params.items_tires = items_tires;
                render_params.total_sum = total_sum;
                res.render('p_basket.pug', render_params);
            });
        });
    } else {
        res.send();
    }
});