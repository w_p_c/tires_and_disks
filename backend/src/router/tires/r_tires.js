"use strict";

const path = require('path');
const express = require('express');
const router = express.Router();
module.exports = router;
const mysql = require("mysql");

const db = require(path.join(__dirname, "/../../database/database.js"));
const q = require(path.join(__dirname, "/../../database/query.js"));
const l = require(path.join(__dirname, "/../../log/logger.js"));

const tires_table = db.table.catalogue_tires;
const tires_table_name = db.db.db_name + "." + tires_table.table_name;

// Get detailed item info
router.get('/tires/:itemid', function (req, res) {
    let render_params = {};

    if (req.session && req.session.basket) {
        render_params.basket_amount = req.session.basket.length;
    }

    if (req.params.itemid) {
        let con = mysql.createConnection(db.customer_con_opts);
        con.connect(function (err_connect) {
            if (err_connect) {
                l.lsql(err_connect);
            }

            con.query(q.fun_select_simple(
                tires_table_name,
                ["*"],
                "idProduct=" + con.escape(req.params.itemid),
                null,
                1
            ), function (err_sel, res_sel) {
                if (err_sel) {
                    l.lsql(err_sel);
                }

                if (res_sel && res_sel[0]) {
                    render_params.item = res_sel[0];
                }
            });

            con.end(function (err_end_con) {
                if (err_end_con) {
                    l.lsql(err_end_con);
                }

                // Basket id
                if (req.session) {
                    render_params.basket_id = [];
                    if (req.session.basket) {
                        for (let i = 0; i < req.session.basket.length; ++i) {
                            render_params.basket_id.push(JSON.parse(req.session.basket[i]).id);
                        }
                    } else {
                        req.session.basket = [];
                    }
                }

                render_params.type = "tires";

                res.render("p_item.pug", render_params);
            })
        });
    }
});

// Select models according to brand
router.post('/tires/select', function (req, res) {
    if (req.query && req.query.brand) {
        let con = mysql.createConnection(db.customer_con_opts);
        con.connect(function (err_connect) {
            if (err_connect) {
                l.lsql(err_connect);
            }

            con.query(q.fun_select_simple(
                tires_table_name,
                ["model"],
                "brand=" + con.escape(req.query.brand),
                "model ASC",
                null,
                true,
                true
            ), function (err_sel_model, res_sel_model) {
                if (err_sel_model) {
                    l.lsql(err_sel_model);
                }

                res.json(res_sel_model);
            });

            con.end(function (err_end) {
                if (err_end) {
                    l.lsql(err_end);
                }
            });
        });
    }
});

router.get('/tires', function (req, res) {
    let render_params = { catalogue_type: 'tires', query: req.query };

    let con = mysql.createConnection(db.customer_con_opts);
    con.connect(function (err_con) {
        if (err_con) {
            l.lsql(err_con);
        }

        let where = "";
        let order_by = "";
        let limit = "";


        if (req.query) {

            // Where - Min price
            if (req.query.min_price) {
                if (where != "") {
                    where += " AND ";
                }
                where += "(end_rozn_price >= " + con.escape(req.query.min_price) + ")";
            }

            // Where - Max price
            if (req.query.max_price) {
                if (where != "") {
                    where += " AND ";
                }
                where += "(end_rozn_price <= " + con.escape(req.query.max_price) + ")";
            }

            // Where - Brand
            if (req.query.brand && req.query.brand != "all") {
                if (where != "") {
                    where += " AND ";
                }
                where += "(brand = " + con.escape(req.query.brand) + ")";
            }

            // Where - Model
            if (req.query.model && req.query.model != "all") {
                if (where != "") {
                    where += " AND ";
                }
                where += "(model = " + con.escape(req.query.model) + ")";
            }

            // Where - Diameter
            if (req.query.diameter && req.query.diameter != "all") {
                if (where != "") {
                    where += " AND ";
                }
                where += "(diameter = " + con.escape(req.query.diameter) + ")";
            }

            // Where - Width
            if (req.query.width && req.query.width != "all") {
                if (where != "") {
                    where += " AND ";
                }
                where += "(width = " + con.escape(req.query.width) + ")";
            }

            // Where - Height
            if (req.query.height && req.query.height != "all") {
                if (where != "") {
                    where += " AND ";
                }
                where += "(height = " + con.escape(req.query.height) + ")";
            }

            // Where - Season
            if (req.query.season_all || req.query.season_summer || req.query.season_winter) {
                if (where != "") {
                    where += " AND ";
                }

                let local_where = "";

                // Where - season_all
                if (req.query.season_all) {
                    if (local_where != "") {
                        local_where += " OR ";
                    }
                    local_where += "(season = " + con.escape(req.query.season_all) + ")";
                }

                // Where - season_summer
                if (req.query.season_summer) {
                    if (local_where != "") {
                        local_where += " OR ";
                    }
                    local_where += "(season = " + con.escape(req.query.season_summer) + ")";
                }

                // Where - season_winter
                if (req.query.season_winter) {
                    if (local_where != "") {
                        local_where += " OR ";
                    }
                    local_where += "(season = " + con.escape(req.query.season_winter) + ")";
                }

                where += "(" + local_where + ")";
            }

            // Order by
            if (req.query.order_by) {
                switch (req.query.order_by) {
                    case 'cheap_first':
                        order_by += "end_rozn_price ASC";
                        break;
                    case 'expensive_first':
                        order_by += "end_rozn_price DESC";
                        break;
                }
            } else {
                order_by += "end_rozn_price ASC";
            }

            // Limit amount of items
            if (req.query.limit) {
                limit = req.query.limit;
            } else {
                limit = 25;
            }
        }


        let querr = q.fun_select_simple(
            tires_table_name,
            ["*"],
            where,
            order_by,
            limit
        );

        console.log(querr);

        // Query for items
        con.query(querr, function (error, results, fields) {
            if (error) {
                l.lsql(error);
            }

            if (results) {
                render_params.items = results;
            } else {
                render_params.items = [];
            }
        });

        // Get constants
        if (req.query) {
            if (req.query.ajax_load_more != "true") {
                query_constants(render_params, con);
            }
        } else {
            query_constants(render_params, con);
        }

        con.end(function (err_con_end) {
            if (err_con_end) {
                l.lsql(err_con_end);
            }

            // Basket id
            if (req.session) {
                render_params.basket_id = [];
                if (req.session.basket) {
                    for (let i = 0; i < req.session.basket.length; ++i) {
                        render_params.basket_id.push(JSON.parse(req.session.basket[i]).id);
                    }
                } else {
                    req.session.basket = [];
                }
            }

            if (req.query.ajax_load_more == "true") {
                res.json([render_params.items, render_params.basket_id]);
            } else {
                res.render('p_catalogue.pug', render_params);
            }
        });
    });
});

function query_constants(render_params, con_loc) {

    // Get min-max price
    con_loc.query(q.fun_select_simple(
        tires_table_name,
        ["MAX(" + tires_table.cols.end_rozn_price.colname + ") as max",
        "MIN(" + tires_table.cols.end_rozn_price.colname + ") as min"]
    ), function (errr_sel_const, res_sel_const) {
        if (errr_sel_const) {
            l.lsql(errr_sel_const);
        }

        if (res_sel_const) {
            // Min-max price
            if (res_sel_const[0]) {
                render_params.max_global_price = Math.ceil(res_sel_const[0].max);
                render_params.min_global_price = Math.floor(res_sel_const[0].min);
            }
        }
    });

    // Get brands
    con_loc.query(q.fun_select_simple(
        tires_table_name,
        [tires_table.cols.brand.colname], null,
        tires_table.cols.brand.colname + " ASC",
        null,
        true,
        true
    ), function (err_s_detail, res_s_par, res_s_field) {
        if (err_s_detail) {
            l.lsql(err_s_detail);
        }

        render_params.a_brand = [];

        if (res_s_par) {
            for (let val of res_s_par) {
                render_params.a_brand.push(val.brand);
            }
        }
    });

    // Get diameters
    con_loc.query(q.fun_select_simple(
        tires_table_name,
        [tires_table.cols.diameter.colname], null,
        tires_table.cols.diameter.colname + " ASC",
        null,
        true,
        true
    ), function (err_s_detail, res_s_par, res_s_field) {
        if (err_s_detail) {
            l.lsql(err_s_detail);
        }

        render_params.a_diameter = [];

        if (res_s_par) {
            for (let val of res_s_par) {
                render_params.a_diameter.push(val.diameter);
            }
        }
    });

    // Get width
    con_loc.query(q.fun_select_simple(
        tires_table_name,
        [tires_table.cols.width.colname], null,
        tires_table.cols.width.colname + " ASC",
        null,
        true,
        true
    ), function (err_s_detail, res_s_par, res_s_field) {
        if (err_s_detail) {
            l.lsql(err_s_detail);
        }

        render_params.a_width = [];

        if (res_s_par) {
            for (let val of res_s_par) {
                render_params.a_width.push(val.width);
            }
        }
    });

    // Get height
    con_loc.query(q.fun_select_simple(
        tires_table_name,
        [tires_table.cols.height.colname], null,
        tires_table.cols.height.colname + " ASC",
        null,
        true,
        true
    ), function (err_s_detail, res_s_par, res_s_field) {
        if (err_s_detail) {
            l.lsql(err_s_detail);
        }

        render_params.a_height = [];

        if (res_s_par) {
            for (let val of res_s_par) {
                render_params.a_height.push(val.height);
            }
        }
    });
};