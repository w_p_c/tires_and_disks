"use strict";

let path = require('path');

let express = require('express');
let router = express.Router();
module.exports = router;

router.get('/pricing', function (req, res) {

    let render_params = {};

    if (req.session && req.session.basket) {
        render_params.basket_amount = req.session.basket.length;
    }

    res.render('p_pricing.pug', render_params);
});