"use strict";

const path = require('path');

const express = require('express');
const router = express.Router();
module.exports = router;

const mysql = require("mysql");

const db = require(path.join(__dirname, "/../../database/database.js"));
const q = require(path.join(__dirname, "/../../database/query.js"));
const l = require(path.join(__dirname, "/../../log/logger.js"));

const disk_table = db.table.catalogue_disks;
const tires_table = db.table.catalogue_tires;
const auto_table = db.table.catalogue_auto;

const disk_table_name = db.db.db_name + "." + disk_table.table_name;
const tires_table_name = db.db.db_name + "." + tires_table.table_name;
const auto_table_name = db.db.db_name + "." + auto_table.table_name;

function parse_row_el_auto(row, type) {
    let temp_split_tube, temp_split_sharp, res_arr = [], val;

    // Tires stock
    temp_split_tube = row.split("|");
    for (let i = 0; i < temp_split_tube.length; ++i) {
        val = temp_split_tube[i];
        val.trim();

        temp_split_sharp = val.split("#");

        if (temp_split_sharp.length == 2) {
            res_arr.push({
                name: temp_split_sharp[0] + " (передняя ось)",
                href: get_href_auto(temp_split_sharp[0], type)
            });

            res_arr.push({
                name: temp_split_sharp[1] + " (задняя ось)",
                href: get_href_auto(temp_split_sharp[1], type)
            });
        } else if (temp_split_sharp.length == 1) {
            res_arr.push({
                name: temp_split_sharp[0],
                href: get_href_auto(temp_split_sharp[0], type)
            });
        }
    }

    return res_arr;
}

const regex_auto_tire = /^([0-9]+)\/([0-9]+)\sR([0-9]+)$/;
const regex_auto_disk = /^([0-9,]+)\sx\s([0-9]+)\sET([0-9]+)$/;
function get_href_auto(literal, type) {
    literal = literal.trim();

    let result = "";
    let match;

    if (type == "disks") {
        result += "/disks?";

        match = literal.match(regex_auto_disk);

        if (match) {
            result += "width=" + match[1]
                + "&diameter=" + match[2]
                + "&et=" + match[3];
        } else {
            result = null;
        }

    } else if (type == "tires") {
        result += "/tires?";

        match = literal.match(regex_auto_tire);

        if (match) {
            result += "width=" + match[1]
                + "&height=" + match[2]
                + "&diameter=" + match[3];
        } else {
            result = null;
        }
    }

    return result;
}

// Get tires/disks filter by auto parameters
router.get("/auto", function (req, res) {
    let render_params = {};
    if (req.query && req.query.vendor && req.query.model && req.query.modif && req.query.year) {
        let con = mysql.createConnection(db.customer_con_opts);
        con.connect(function (err_connect) {
            if (err_connect) {
                l.lsql(err_connect);
            }

            con.query(q.fun_select_simple(
                auto_table_name,
                ["*"],
                "(vendor=" + con.escape(req.query.vendor)
                + ") AND (car=" + con.escape(req.query.model)
                + ") AND (modification=" + con.escape(req.query.modif)
                + ") AND (year=" + con.escape(req.query.year) + ")",
                null,
                1
            ), function (err_sel, res_sel) {
                if (err_sel) {
                    l.lsql(err_sel);
                }

                if (res_sel && res_sel[0]) {
                    render_params.row = res_sel[0];

                    render_params.tires_stock = res_sel[0].zavod_shini ? parse_row_el_auto(res_sel[0].zavod_shini, "tires") : null;
                    render_params.tires_replace = res_sel[0].zamen_shini ? parse_row_el_auto(res_sel[0].zamen_shini, "tires") : null;
                    render_params.tires_tuning = res_sel[0].tuning_shini ? parse_row_el_auto(res_sel[0].tuning_shini, "tires") : null;

                    render_params.disks_stock = res_sel[0].zavod_diskov ? parse_row_el_auto(res_sel[0].zavod_diskov, "disks") : null;
                    render_params.disks_replace = res_sel[0].zamen_diskov ? parse_row_el_auto(res_sel[0].zamen_diskov, "disks") : null;
                    render_params.disks_tuning = res_sel[0].tuning_diski ? parse_row_el_auto(res_sel[0].tuning_diski, "disks") : null;

                    render_params.query = req.query;

                    res.render("p_auto.pug", render_params);
                } else {
                    res.send("error2");
                }
            });
        });
    } else {
        res.send("error1");
    }
});