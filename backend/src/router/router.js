"use strict";

const path = require('path');

const logger = require(path.join(__dirname, "/../log/logger.js"));

logger.info("! Express router enabled");

const express = require('express');
const app = express();
const session = require('express-session');

const ENVIRONMENT = ["production", "testing"][1];

const frontendPath = path.join(__dirname, '/../../../frontend/');

//// EXPRESS ROUTER
//  View engine
app.set('views', path.join(frontendPath, 'html/'));
app.set('view engine', 'pug');

//  Static path
app.use(express.static(path.join(frontendPath, 'css/')));
app.use(express.static(path.join(frontendPath, 'js/')));
app.use(express.static(path.join(frontendPath, 'img/')));
app.use(express.static(path.join(frontendPath, 'fonts/')));

//  Add-on middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//  App-session setup
let app_session_settings = {
    secret: 'CFJkW6QXV7AkYmxnXXtNVxnu',
    resave: false,
    saveUninitialized: false,
    unset: 'destroy',
    cookie: {
        secure: false,
        maxAge: 60 * 60000 // 60 min
    }
};
if (ENVIRONMENT == "production") {
    app_session_settings.cookie.secure = true;
}
app.use(session(app_session_settings));

// Upload files
const fileUpload = require('express-fileupload');
app.use(fileUpload({
    limits: { fileSize: 50 * 1024 * 1024 },
}));

const blocked_path = [
    "/current_config/passwd",
    "/HNAP1/",
    "/null"
];
//  Own middleware
app.use(function (req, res, next) {
    logger.info("Method: " + req.method
        + " <||> IP: " + (req.headers['x-forwarded-for'] || req.connection.remoteAddress)
        + " <||> Request: " + req.originalUrl);

    if (blocked_path.includes(req.originalUrl)) {
        res.send("Fuck YOU DICKHEAD!!!");
    } else {
        if (!req.session.basket) {
            req.session.basket = [];
            logger.info("Basket initialized! Ssid: " + req.session.id);
        }

        next();
    }
});

//  Routers setup
let r_main = require(path.join(__dirname + '/main/r_main'));
app.use('/', r_main);

app.use(function (req, res, next) {
    res.status(404);

    // respond with html page
    if (req.accepts('html')) {
        let render_params = {};

        if (req.session && req.session.basket) {
            render_params.basket_amount = req.session.basket.length;
        }

        res.render('p_404.pug', render_params);
        return;
    }

    // respond with json
    if (req.accepts('json')) {
        res.send({ error: 'Not found' });
        return;
    }

    // default to plain-text. send()
    res.type('txt').send('Not found');
});

app.listen(80);