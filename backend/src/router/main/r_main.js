"use strict";

const path = require('path');

const express = require('express');
const router = express.Router();
module.exports = router;

const r_about = require(path.join(__dirname, '/../about/r_about'));
const r_disks = require(path.join(__dirname, '/../disks/r_disks'));
const r_pricing = require(path.join(__dirname, '/../pricing/r_pricing'));
const r_tires = require(path.join(__dirname, '/../tires/r_tires'));
const r_admin = require(path.join(__dirname, '/../admin/r_admin'));
const r_basket = require(path.join(__dirname, '/../basket/r_basket'));
const r_auto = require(path.join(__dirname, "/../auto/r_auto"));

router.use(r_about);
router.use(r_disks);
router.use(r_pricing);
router.use(r_tires);
router.use(r_admin);
router.use(r_basket);
router.use(r_auto);

router.get('/', function (req, res) {
    res.redirect('/main');
});


const mysql = require("mysql");

const db = require(path.join(__dirname, "/../../database/database.js"));
const q = require(path.join(__dirname, "/../../database/query.js"));
const l = require(path.join(__dirname, "/../../log/logger.js"));

const disk_table = db.table.catalogue_disks;
const tires_table = db.table.catalogue_tires;
const auto_table = db.table.catalogue_auto;

const disk_table_name = db.db.db_name + "." + disk_table.table_name;
const tires_table_name = db.db.db_name + "." + tires_table.table_name;
const auto_table_name = db.db.db_name + "." + auto_table.table_name;

// Select models according to vendor
router.post('/main/select', function (req, res) {

    if (req.query) {
        let where = "";
        let col = "";

        if (req.query.vendor) {
            if (where != "") {
                where += " AND ";
            }
            where += "(vendor=" + mysql.escape(req.query.vendor) + ")";
            col = "car";

        }

        if (req.query.model) {
            if (where != "") {
                where += " AND ";
            }
            where += "(car=" + mysql.escape(req.query.model) + ")";
            col = "modification";
        }

        if (req.query.modif) {
            if (where != "") {
                where += " AND ";
            }
            where += "(modification=" + mysql.escape(req.query.modif) + ")";
            col = "year";
        }

        if (where != "" && col != "") {
            let con = mysql.createConnection(db.customer_con_opts);
            con.connect(function (err_connect) {
                if (err_connect) {
                    l.lsql(err_connect);
                }

                con.query(q.fun_select_simple(
                    auto_table_name,
                    [col],
                    where,
                    col + " ASC",
                    null,
                    true,
                    true), function (err_sel, res_sel) {
                        if (err_sel) {
                            l.lsql(err_sel);
                        }

                        res.json(res_sel);
                    });

                con.end(function (err_end) {
                    if (err_end) {
                        l.lsql(err_end);
                    }
                });
            });
        }
    }
});

router.get('/main', function (req, res) {

    let render_params = {};

    if (req.session && req.session.basket) {
        render_params.basket_amount = req.session.basket.length;
    }

    let con = mysql.createConnection(db.customer_con_opts);
    con.connect(function (err_con) {
        if (err_con) {
            l.lsql(err_con);
        }

        query_tires_constants(render_params, con);
        query_disks_constants(render_params, con);
        query_auto_constants(render_params, con);

        con.end(function (con_end_err) {
            if (con_end_err) {
                l.lsql(con_end_err);
            }

            res.render('p_main.pug', render_params);
        });
    });
});

// Auto constants
function query_auto_constants(render_params, con_loc) {

    // Get vendor
    con_loc.query(q.fun_select_simple(
        auto_table_name,
        [auto_table.cols.vendor.colname],
        null,
        auto_table.cols.vendor.colname + " ASC",
        null, true, true
    ), function (err, res) {
        if (err) {
            l.lsql(err);
        }

        render_params.auto_a_vendor = [];
        if (res) {
            for (let val of res) {
                render_params.auto_a_vendor.push(val.vendor);
            }
        }
    });
}

// Tires cnstants
function query_tires_constants(render_params, con_loc) {
    // Get width
    con_loc.query(q.fun_select_simple(
        tires_table_name,
        [tires_table.cols.width.colname], null,
        tires_table.cols.width.colname + " ASC",
        null,
        true,
        true
    ), function (err_s_detail, res_s_par, res_s_field) {
        if (err_s_detail) {
            l.lsql(err_s_detail);
        }

        render_params.tires_a_width = [];

        if (res_s_par) {
            for (let val of res_s_par) {
                render_params.tires_a_width.push(val.width);
            }
        }
    });

    // Get height
    con_loc.query(q.fun_select_simple(
        tires_table_name,
        [tires_table.cols.height.colname], null,
        tires_table.cols.height.colname + " ASC",
        null,
        true,
        true
    ), function (err_s_detail, res_s_par, res_s_field) {
        if (err_s_detail) {
            l.lsql(err_s_detail);
        }

        render_params.tires_a_height = [];

        if (res_s_par) {
            for (let val of res_s_par) {
                render_params.tires_a_height.push(val.height);
            }
        }
    });

    // Get diameters
    con_loc.query(q.fun_select_simple(
        tires_table_name,
        [tires_table.cols.diameter.colname], null,
        tires_table.cols.diameter.colname + " ASC",
        null,
        true,
        true
    ), function (err_s_detail, res_s_par, res_s_field) {
        if (err_s_detail) {
            l.lsql(err_s_detail);
        }

        render_params.tires_a_diameter = [];

        if (res_s_par) {
            for (let val of res_s_par) {
                render_params.tires_a_diameter.push(val.diameter);
            }
        }
    });

    // Get brands
    con_loc.query(q.fun_select_simple(
        tires_table_name,
        [tires_table.cols.brand.colname], null,
        tires_table.cols.brand.colname + " ASC",
        null,
        true,
        true
    ), function (err_s_detail, res_s_par, res_s_field) {
        if (err_s_detail) {
            l.lsql(err_s_detail);
        }

        render_params.tires_a_brand = [];

        if (res_s_par) {
            for (let val of res_s_par) {
                render_params.tires_a_brand.push(val.brand);
            }
        }
    });
}

// Disks constants
function query_disks_constants(render_params, con_loc) {

    // Get krepezh
    con_loc.query(q.fun_select_simple(
        disk_table_name,
        [disk_table.cols.krepezh.colname], null,
        disk_table.cols.krepezh.colname + " ASC",
        null,
        true,
        true
    ), function (err_s_detail, res_s_par, res_s_field) {
        if (err_s_detail) {
            l.lsql(err_s_detail);
        }

        render_params.disks_a_krepezh = [];

        if (res_s_par) {
            for (let val of res_s_par) {
                render_params.disks_a_krepezh.push(val.krepezh);
            }
        }
    });

    // Get diameters
    con_loc.query(q.fun_select_simple(
        disk_table_name,
        [disk_table.cols.diameter.colname], null,
        disk_table.cols.diameter.colname + " ASC",
        null,
        true,
        true
    ), function (err_s_detail, res_s_par, res_s_field) {
        if (err_s_detail) {
            l.lsql(err_s_detail);
        }

        render_params.disks_a_diameter = [];

        if (res_s_par) {
            for (let val of res_s_par) {
                render_params.disks_a_diameter.push(val.diameter);
            }
        }
    });

    // Get pcd1
    con_loc.query(q.fun_select_simple(
        disk_table_name,
        [disk_table.cols.pcd1.colname], null,
        disk_table.cols.pcd1.colname + " ASC",
        null,
        true,
        true
    ), function (err_s_detail, res_s_par, res_s_field) {
        if (err_s_detail) {
            l.lsql(err_s_detail);
        }

        render_params.disks_a_pcd1 = [];

        if (res_s_par) {
            for (let val of res_s_par) {
                render_params.disks_a_pcd1.push(val.pcd1);
            }
        }
    });

    // Get brands
    con_loc.query(q.fun_select_simple(
        disk_table_name,
        [disk_table.cols.brand.colname], null,
        disk_table.cols.brand.colname + " ASC",
        null,
        true,
        true
    ), function (err_s_detail, res_s_par, res_s_field) {
        if (err_s_detail) {
            l.lsql(err_s_detail);
        }

        render_params.disks_a_brand = [];

        if (res_s_par) {
            for (let val of res_s_par) {
                render_params.disks_a_brand.push(val.brand);
            }
        }
    });
}