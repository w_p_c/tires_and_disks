"use strict";

const path = require('path');

const express = require('express');
const router = express.Router();
module.exports = router;

const db = require(path.join(__dirname, '/../../database/database'));
const mysql = require('mysql');
const query = require(path.join(__dirname, '/../../database/query'));

const disk_table = db.table.catalogue_disks;
const tires_table = db.table.catalogue_tires;
const orders_table = db.table.orders_list;

const disk_table_name = db.db.db_name + "." + disk_table.table_name;
const tires_table_name = db.db.db_name + "." + tires_table.table_name;
const orders_table_name = db.db.db_name + "." + orders_table.table_name;

const dateformat = require('dateformat');

const l = require(path.join(__dirname, "/../../log/logger.js"));

const regex_urlQuery = /.+(\?.+)/;

const upload_files_dir = path.join(__dirname, '/../../../data/upload/');
//  Upload files to refresh DB
router.post('/admin/upload', function (req, res) {
    if (req.session.auth) {
        if (!req.files) {
            l.info("! UPLOAD: No files were uploaded");
            return res.status(400).send('No files were uploaded.');
        } else {
            let con = mysql.createConnection(db.shop_owner_con_opts);
            con.connect(function (err_connect) {
                if (err_connect) {
                    l.lsql(err_connect);
                }

                // File disks
                if (req.files.file_disks) {
                    req.files.file_disks.mv(upload_files_dir + "file_disks.xls", function (err_disks) {
                        if (err_disks) {
                            l.e(err_disks);
                        }

                        l.info("! UPLOAD: 'file_disks.xls' uploaded and moved");

                        res.send("done");

                        db.refresh_db_data_from_xls(upload_files_dir + "file_disks.xls",
                            disk_table,
                            disk_table_name,
                            con,
                            db.not_concat_vals.catalogue_disks);

                        con.end(function (err_con_end) {
                            if (err_con_end) {
                                l.lsql(err_con_end);
                            }

                            l.info("! UPLOAD: SQL refreshing 'file_disks.xls' ended");
                        });
                    });
                } else if (req.files.file_tires_gruz) {
                    req.files.file_tires_gruz.mv(upload_files_dir + "file_tires_gruz.xls", function (err_disks) {
                        if (err_disks) {
                            l.e(err_disks);
                        }

                        l.info("! UPLOAD: 'file_tires_gruz.xls' uploaded and moved");

                        res.send("done");

                        db.refresh_db_data_from_xls(upload_files_dir + "file_tires_gruz.xls",
                            tires_table,
                            tires_table_name,
                            con,
                            db.not_concat_vals.catalogue_tires);

                        con.end(function (err_con_end) {
                            if (err_con_end) {
                                l.lsql(err_con_end);
                            }

                            l.info("! UPLOAD: SQL refreshing 'file_tires_gruz.xls' ended");
                        });
                    });
                } else if (req.files.file_tires_legk) {
                    req.files.file_tires_legk.mv(upload_files_dir + "file_tires_legk.xls", function (err_disks) {
                        if (err_disks) {
                            l.e(err_disks);
                        }

                        l.info("! UPLOAD: 'file_tires_legk.xls' uploaded and moved");

                        res.send("done");

                        db.refresh_db_data_from_xls(upload_files_dir + "file_tires_legk.xls",
                            tires_table,
                            tires_table_name,
                            con,
                            db.not_concat_vals.catalogue_tires);

                        con.end(function (err_con_end) {
                            if (err_con_end) {
                                l.lsql(err_con_end);
                            }

                            l.info("! UPLOAD: SQL refreshing 'file_tires_legk.xls' ended");
                        });
                    });
                } else {
                    res.send("error-no-files");
                }
            });
        }
    } else {
        res.send("error-auth");
    }
});

//  Change order status
router.get('/admin/change', function (req, res) {
    let ordersTypeRedirect;

    if (req.session.auth) {
        if (req.query && req.query.idOrder && req.query.new_status) {
            if (req.query.new_status == "pending" || req.query.new_status == "closed" || req.query.new_status == "cancelled") {
                let connection = mysql.createConnection(db.shop_owner_con_opts);
                connection.connect(function (err_con) {
                    if (err_con) {
                        l.lsql(err_con);
                    }

                    connection.query(query.fun_update_where(orders_table_name,
                        ["order_status"], ["\'" + req.query.new_status + "\'"], "idOrder=" + req.query.idOrder),
                        function (error, results, fields) {
                            if (error) l.lsql(error);
                        });

                    connection.end();
                });

                if (req.query.orders_type) {
                    ordersTypeRedirect = '/orders/' + req.query.orders_type;
                }
            }
        }
    }

    res.redirect('/admin' + (ordersTypeRedirect ? ordersTypeRedirect : ''));
});

//  Get order by number
router.get('/admin/orders/number', function (req, res) {
    res.redirect('/admin');
});

//  Get order by number REDIRECT
router.get('/admin/orders/number/(:number)?', function (req, res) {
    if (req.session.auth) {
        if (req.params.number) {
            select_orders_and_render("idOrder=" + req.params.number,
                "dd-mm-yyyy", req, res, "p_admin_panel.pug", { orderNumber: req.params.number });
        } else {
            res.redirect('/admin');
        }
    } else {
        res.redirect('/admin');
    }
});

//  Sort orders
router.get('/admin/orders/:type/:page/sort', function (req, res) {
    if (req.session.auth) {
        let where = '';
        let ordersStatus;

        switch (req.params.type) {
            case "pending":
                ordersStatus = "pending";
                where += "(order_status=\'pending\')";
                break;
            case "closed":
                where += "(order_status=\'closed\')";
                ordersStatus = "closed";
                break;
            case "cancelled":
                where += "(order_status=\'cancelled\')";
                ordersStatus = "cancelled";
                break;
        }

        if (req.query) {
            if (req.query.date_from) {
                where += " AND (date_sent >= \'" + req.query.date_from + "\')";
            }

            if (req.query.date_to) {
                where += " AND (date_sent <= \'" + req.query.date_to + "\')";
            }

            if (req.query.order_by) {
                switch (req.query.order_by) {
                    case 'old_first':
                        where += " ORDER BY date_sent ASC";
                        break;
                    case 'new_first':
                        where += " ORDER BY date_sent DESC";
                        break;
                }
            }
        }

        select_orders_and_render(where, "dd-mm-yyyy", req, res,
            'p_admin_panel.pug', { type: ordersStatus, query: req.query },
            "/admin/orders/" + req.params.type + "/",
            "/sort" + regex_urlQuery.exec(req.originalUrl)[1],
            req.params.page, req.query.elements_on_page);
    } else {
        res.redirect('/admin');
    }
});

//  Get orders by type
router.get('/admin/orders/:type/:page', function (req, res) {
    if (req.session.auth) {
        let ordersStatus;

        switch (req.params.type) {
            case "pending":
                ordersStatus = "pending";
                break;
            case "closed":
                ordersStatus = "closed";
                break;
            case "cancelled":
                ordersStatus = "cancelled";
                break;
        }

        select_orders_and_render("order_status=\'" + ordersStatus + "\' ORDER BY date_sent DESC",
            "dd-mm-yyyy HH:MM", req, res, 'p_admin_panel.pug', { type: ordersStatus },
            "/admin/orders/" + req.params.type + "/", "",
            req.params.page);

    } else {
        res.redirect('/admin');
    }
});

//  Get orders REDIRECT
router.get('/admin/orders/:type', function (req, res) {
    res.redirect('/admin/orders/' + req.params.type + '/1');
});
router.get('/admin/orders', function (req, res) {
    res.redirect('/admin/orders/pending/1');
});


//  Logout
router.get('/admin/logout', function (req, res) {
    req.session.cookie.maxAge = 0;
    l.info('\'/admin/logout\' (post) session-id: ' + req.session.id + ' expires: ' + req.session.cookie.expires + ' auth: ' + req.session.auth);
    res.redirect('/admin');
});

//  Admin page
router.get('/admin', function (req, res) {
    l.info('\'/admin\' (get) session-id: ' + req.session.id + ' expires: ' + req.session.cookie.expires);

    if (req.session.auth) {
        res.redirect('/admin/orders/pending');
    } else {
        res.render('p_admin_login.pug');
    }
});

//  Admin auth form
router.post('/admin', function (req, res) {
    if (req.body.password === db.user.shop_owner.admin_panel_password) {
        req.session.auth = true;
    } else {
        req.session.auth = false;
    }

    l.info('\'/admin\' (post) session-id: ' + req.session.id + ' expires: ' + req.session.cookie.expires + ' auth: ' + req.session.auth);

    res.redirect('/admin');
});

function select_orders_and_render(where, formatDateResults, req, res, pug_page, pug_params,
    url_before_page, url_after_page, page = 1, elements_on_page = 25) {

    let connection = mysql.createConnection(db.shop_owner_con_opts);
    connection.connect(function (err_con) {
        if (err_con) {
            l.lsql(err_con);
        }

        let belowMarginPage, aboveMarginPage, sliceResultsEnd, pages_numbers, pages_amount;

        connection.query(query.fun_select_simple(
            orders_table_name,
            ['*'], where
        ), function (error, results, fields) {
            if (error) {
                l.lsql(error);
            }

            if (!results) {
                results = [];
            }

            if (formatDateResults) {
                for (let i = 0; i < results.length; ++i) {
                    results[i].date_sent = dateformat(results[i].date_sent, formatDateResults);
                    try {
                        results[i].order_positions = JSON.parse(results[i].order_positions);
                    } catch (e) {
                        l.e(e);
                    }
                }
            }

            page = Number.parseInt(page);

            sliceResultsEnd = page * elements_on_page;

            pug_params.orders = results.slice(sliceResultsEnd - elements_on_page, sliceResultsEnd);
            pug_params.results_amount = results.length;
            pug_params.elements_on_page = elements_on_page;

            if (page <= 5 && 1) {
                belowMarginPage = 1;
            } else if (page > 5) {
                belowMarginPage = page - 5;
            }
            pages_amount = Math.ceil(results.length / elements_on_page);

            aboveMarginPage = page + 5;

            if (aboveMarginPage < 10) {
                aboveMarginPage = 10;
            }

            if (aboveMarginPage > pages_amount) {
                aboveMarginPage -= (aboveMarginPage - pages_amount);
            }

            pug_params.above_margin_page = aboveMarginPage;
            pug_params.below_margin_page = belowMarginPage;

            pug_params.url_before_page = url_before_page;
            pug_params.url_after_page = url_after_page;

            pug_params.page = page;

            res.render(pug_page, pug_params);
        });

        connection.end(function (err_connnection_end) {
            if (err_connnection_end) {
                l.lsql(err_connnection_end);
            }
        });
    });
};