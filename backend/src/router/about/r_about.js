"use strict";

let express = require('express');
let router = express.Router();

router.get('/about', function (req, res) {

    let render_params = {};

    if (req.session && req.session.basket) {
        render_params.basket_amount = req.session.basket.length;
    }

    res.render('p_about.pug', render_params);
})

module.exports = router;