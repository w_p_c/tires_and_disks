"use strict";

const path = require('path');

const mysql = require('mysql');
const xlsx = require('xlsx');

const logger = require(path.join(__dirname, "/../log/logger.js"));;

let user = {
    root: {
        name: 'root',
        password: 'sgalaxys2',
        host: 'localhost'
    },
    customer: {
        name: 'customer',
        password: 'hKhrmK2CZmfnsB9xg7pPSXH5',
        host: 'localhost',
        privileges: ['ALL']
    },
    shop_owner: {
        name: 'shop_owner',
        password: 'FsZn759QL2K8s8ahdpTS6y3H',
        host: 'localhost',
        privileges: ['ALL'],
        admin_panel_password: "tires_MDA_qoob"
    }
};
module.exports.user = user;

let db = {
    db_name: 'db_tires_disks'
};
module.exports.db = db;

let table = {
    catalogue_tires: {
        table_name: 'catalogue_tires',
        default_coltype: 'varchar(300) NOT NULL',
        cols: {
            idProduct: {
                colname: 'idProduct',
                coltype: 'int(15) NOT NULL'
            },
            brand: {
                colname: 'brand'
            },
            height: {
                colname: 'height',
                coltype: 'double'
            },
            end_opt_price: {
                colname: 'end_opt_price',
                coltype: 'double'
            },
            end_rozn_price: {
                colname: 'end_rozn_price',
                coltype: 'double'
            },
            year: {
                colname: 'year'
            },
            supplier_city: {
                colname: 'supplier_city'
            },
            date: {
                colname: 'date'
            },
            diameter: {
                colname: 'diameter',
                coltype: 'double'
            },
            load_index: {
                colname: 'load_index'
            },
            speed_index: {
                colname: 'speed_index'
            },
            model: {
                colname: 'model'
            },
            name: {
                colname: 'name'
            },
            ini_opt_price: {
                colname: 'ini_opt_price',
                coltype: 'double'
            },
            amount: {
                colname: 'amount'
            },
            supplier: {
                colname: 'supplier'
            },
            ini_rozn_price: {
                colname: 'ini_rozn_price',
                coltype: 'double'
            },
            season: {
                colname: 'season'
            },
            country: {
                colname: 'country'
            },
            wehicle_type: {
                colname: 'wehicle_type'
            },
            reinforced: {
                colname: 'reinforced'
            },
            photo: {
                colname: 'photo'
            },
            ship: {
                colname: 'ship'
            },
            width: {
                colname: 'width',
                coltype: 'double'
            }
        }
    },
    catalogue_disks: {
        table_name: 'catalogue_disks',
        default_coltype: 'varchar(300) NOT NULL',
        cols: {
            idProduct: {
                colname: 'idProduct',
                coltype: 'int(15) NOT NULL'
            },
            pcd1: {
                colname: 'pcd1',
                coltype: 'double'
            },
            pcd2: {
                colname: 'pcd2',
                coltype: 'double'
            },
            brand: {
                colname: 'brand'
            },
            et: {
                colname: 'et'
            },
            end_opt_price: {
                colname: 'end_opt_price',
                coltype: 'double'
            },
            end_rozn_price: {
                colname: 'end_rozn_price',
                coltype: 'double'
            },
            supplier_city: {
                colname: 'supplier_city'
            },
            date: {
                colname: 'date'
            },
            diameter: {
                colname: 'diameter',
                coltype: 'double'
            },
            dia: {
                colname: 'dia'
            },
            krepezh: {
                colname: 'krepezh',
                coltype: 'int(15)'
            },
            model: {
                colname: 'model'
            },
            name: {
                colname: 'name'
            },
            ini_opt_price: {
                colname: 'ini_opt_price',
                coltype: 'double'
            },
            amount: {
                colname: 'amount'
            },
            supplier: {
                colname: 'supplier'
            },
            ini_rozn_price: {
                colname: 'ini_rozn_price',
                coltype: 'double'
            },
            wheel_type: {
                colname: 'wheel_type'
            },
            photo_1: {
                colname: 'photo_1'
            },
            photo_2: {
                colname: 'photo_2'
            },
            photo_3: {
                colname: 'photo_3'
            },
            color: {
                colname: 'color'
            },
            width: {
                colname: 'width'
            }
        }
    },
    catalogue_auto: {
        table_name: 'catalogue_auto',
        default_coltype: 'varchar(300) NOT NULL',
        cols: {
            id: {
                colname: 'id',
                coltype: 'int(15) NOT NULL'
            },
            vendor: {
                colname: 'vendor'
            },
            car: {
                colname: 'car'
            },
            year: {
                colname: 'year'
            },
            modification: {
                colname: 'modification'
            },
            pcd: {
                colname: 'pcd'
            },
            diameter: {
                colname: 'diameter'
            },
            gaika: {
                colname: 'gaika'
            },
            zavod_shini: {
                colname: 'zavod_shini'
            },
            zamen_shini: {
                colname: 'zamen_shini'
            },
            tuning_shini: {
                colname: 'tuning_shini'
            },
            zavod_diskov: {
                colname: 'zavod_diskov'
            },
            zamen_diskov: {
                colname: 'zamen_diskov'
            },
            tuning_diski: {
                colname: 'tuning_diski'
            }
        }
    },
    orders_list: {
        table_name: 'orders_list',
        default_coltype: 'varchar(300) NOT NULL',
        cols: {
            idOrder: {
                colname: 'idOrder',
                coltype: 'int(15) NOT NULL AUTO_INCREMENT'
            },
            date_sent: {
                colname: 'date_sent',
                coltype: 'DATETIME NOT NULL'
            },
            customer_name: {
                colname: 'customer_name'
            },
            customer_email: {
                colname: 'customer_email'
            },
            customer_telephone: {
                colname: 'customer_telephone'
            },
            shipping_address: {
                colname: 'shipping_address'
            },
            shipping_type: {
                colname: 'shipping_type'
            },
            payment_type: {
                colname: 'payment_type'
            },
            order_positions: {
                colname: 'order_positions',
                coltype: 'TEXT NOT NULL'
            },
            order_status: {
                colname: 'order_status'
            },
            total_sum: {
                colname: 'total_sum'
            }
        }
    }
};
module.exports.table = table;

let not_concat_vals = {
    catalogue_auto: [table.catalogue_auto.cols.id.colname],
    catalogue_disks: [table.catalogue_disks.cols.idProduct.colname,
    table.catalogue_disks.cols.ini_opt_price.colname,
    table.catalogue_disks.cols.ini_rozn_price.colname,
    table.catalogue_disks.cols.end_opt_price.colname,
    table.catalogue_disks.cols.end_rozn_price.colname,
    table.catalogue_disks.cols.diameter.colname,
    table.catalogue_disks.cols.krepezh.colname,
    table.catalogue_disks.cols.pcd1.colname,
    table.catalogue_disks.cols.pcd2.colname],
    catalogue_tires: [table.catalogue_tires.cols.idProduct.colname,
    table.catalogue_tires.cols.ini_opt_price.colname,
    table.catalogue_tires.cols.ini_rozn_price.colname,
    table.catalogue_tires.cols.end_opt_price.colname,
    table.catalogue_tires.cols.end_rozn_price.colname,
    table.catalogue_tires.cols.width.colname,
    table.catalogue_tires.cols.height.colname,
    table.catalogue_tires.cols.diameter.colname]

}
module.exports.not_concat_vals = not_concat_vals;

const query = require(path.join(__dirname + '/query'));
module.exports.query = query;

const root_con_opts = {
    host: user.root.host,
    user: user.root.name,
    password: user.root.password,
    connectTimeout: 0
};
module.exports.root_con_opts = root_con_opts;

const customer_con_opts = {
    host: user.customer.host,
    user: user.customer.name,
    password: user.customer.password,
    connectTimeout: 0
}
module.exports.customer_con_opts = customer_con_opts;

const shop_owner_con_opts = {
    host: user.shop_owner.host,
    user: user.shop_owner.name,
    password: user.shop_owner.password,
    connectTimeout: 0
}
module.exports.shop_owner_con_opts = shop_owner_con_opts;

const main_con = mysql.createConnection(root_con_opts);
main_con.connect(function (err) {
    if (err) {
        logger.lsql(err);
    } else {
        logger.info("! SQL: MySQL (root) connected");

        if (process.argv.includes("drop")) {
            drop_db(main_con);
        } else {
            init(main_con);
        }
    }
});

function init(loc_con) {

    // Create database if not exists
    loc_con.query(query.fun_create_db_if_not_exists(db),
        function (error, results, fields) {
            if (error) {
                logger.lsql(error);
            }

            // Select database
            loc_con.query(query.fun_select_db(db),
                function (error, results, fields) {
                    if (error) logger.lsql(error);

                    //  Create tables if not exists
                    loc_con.query(query.fun_create_table_if_not_exists(
                        table.catalogue_tires.table_name,
                        table.catalogue_tires,
                        [table.catalogue_tires.cols.idProduct]
                    ), function (error, results, fields) {
                        if (error) logger.lsql(error);
                    });
                    loc_con.query(query.fun_create_table_if_not_exists(
                        table.catalogue_disks.table_name,
                        table.catalogue_disks,
                        [table.catalogue_disks.cols.idProduct]
                    ), function (error, results, fields) {
                        if (error) logger.lsql(error);
                    });
                    loc_con.query(query.fun_create_table_if_not_exists(
                        table.catalogue_auto.table_name,
                        table.catalogue_auto,
                        [table.catalogue_auto.cols.id]
                    ), function (error, results, fields) {
                        if (error) logger.lsql(error);
                    });
                    loc_con.query(query.fun_create_table_if_not_exists(
                        table.orders_list.table_name,
                        table.orders_list,
                        [table.orders_list.cols.idOrder]
                    ), function (error, results, fields) {
                        if (error) logger.lsql(error);
                    });

                    // Drop and re-create users
                    loc_con.query(query.drop_user(user.customer),
                        function (err_drop_usr, res_drop_usr, field_drop_usr) {
                            if (err_drop_usr) {
                                logger.lsql(err_drop_usr);
                            }

                            // Drop and re-create users
                            loc_con.query(query.drop_user(user.shop_owner),
                                function (err_drop_usr, res_drop_usr, field_drop_usr) {
                                    if (err_drop_usr) {
                                        logger.lsql(err_drop_usr);
                                    }

                                    // Create user
                                    loc_con.query(query.fun_create_user_and_grant(db, user.shop_owner),
                                        function (error, results, fields) {
                                            if (error) logger.lsql(error);
                                        });

                                    //  Create user
                                    loc_con.query(query.fun_create_user_and_grant(db, user.customer),
                                        function (error, results, fields) {
                                            if (error) logger.lsql(error);
                                        });

                                    loc_con.end(function (err) {
                                        if (err) {
                                            logger.lsql(error);
                                        }

                                        if (process.argv.includes("refresh")) {
                                            let loc_loc_con = mysql.createConnection(root_con_opts);

                                            // Select database
                                            loc_loc_con.query(query.fun_select_db(db),
                                                function (error, results, fields) {
                                                    if (error) logger.lsql(error);

                                                    refresh_db(loc_loc_con);

                                                    loc_loc_con.end(function (err_lol_loc_end) {
                                                        if (err_lol_loc_end) {
                                                            logger.lsql(err_lol_loc_end);
                                                        }

                                                        logger.info("! SQL: Initialized DB");
                                                        
                                                        if (process.argv.includes("refresh")) {
                                                            logger.info("! SQL: Refreshed DB");
                                                        }
                                                    });
                                                });
                                        }
                                    });
                                });

                        });
                });
        });
};

let CONST = {
    min_price_disks: null,
    max_price_disks: null,
    min_price_tires: null,
    max_price_tires: null
}
module.exports.CONST = CONST;
function refresh_db(loc_con) {
    // Refresh data
    refreshDBDataFromXLS(path.join(__dirname + '../../../data/init/podbor.xlsx'),
        table.catalogue_auto,
        table.catalogue_auto.table_name,
        loc_con,
        not_concat_vals.catalogue_auto, false);
    refreshDBDataFromXLS(path.join(__dirname + '../../../data/init/price_diski.xls'),
        table.catalogue_disks,
        table.catalogue_disks.table_name,
        loc_con,
        not_concat_vals.catalogue_disks);
    refreshDBDataFromXLS(path.join(__dirname + '../../../data/init/price_shini_gruz.xls'),
        table.catalogue_tires,
        table.catalogue_tires.table_name,
        loc_con,
        not_concat_vals.catalogue_tires);
    refreshDBDataFromXLS(path.join(__dirname + '../../../data/init/price_shini_legk.xls'),
        table.catalogue_tires,
        table.catalogue_tires.table_name,
        loc_con,
        not_concat_vals.catalogue_tires);

    logger.info("! SQL: Initialized DB refreshing");
};
module.exports.refresh_db = refresh_db;

function drop_db(loc_con) {
    loc_con.query(query.drop_db_if_exists(db),
        function (error, res, fields) {
            if (error) {
                logger.lsql(error);
            }
            logger.info("! SQL: Dropped DB");
        });
};

function refreshDBDataFromXLS(xls_path, table_local, table_name, connection, notConcatValsArray, replaceCommas = true) {

    let startRefreshTime = new Date().getTime();

    logger.info('! SQL: \'' + table_name + '\' table referesh initializing started from \'' + xls_path + '\'');

    //  Initializing .xls workbook
    let workbook = xlsx.readFile(xls_path, { WTF: true });
    let first_sheet = workbook.Sheets[workbook.SheetNames[0]];

    let table_cols_keys = Object.keys(table_local.cols);

    let encodedRow, encodedCell, rowBuffer = [];

    for (let r = 1; ; ++r) {
        encodedRow = xlsx.utils.encode_cell({ r: r, c: 0 });

        if (first_sheet[encodedRow]) {
            for (let c = 0; c < table_cols_keys.length; ++c) {
                encodedCell = first_sheet[xlsx.utils.encode_cell({ c: c, r: r })];

                if (encodedCell) {
                    if (notConcatValsArray.includes(table_cols_keys[c])) {
                        if (typeof encodedCell.v == "string") {
                            encodedCell.v = encodedCell.v.replace("'", "\\'");
                        }

                        if (replaceCommas) {
                            if (typeof encodedCell.v == "string") {
                                rowBuffer.push(encodedCell.v.replace(',', '.'));
                            } else {
                                rowBuffer.push(encodedCell.v);
                            }
                        } else {
                            rowBuffer.push(encodedCell.v);
                        }
                    } else {
                        rowBuffer.push(connection.escape(encodedCell.v));
                    }
                } else {
                    if (notConcatValsArray.includes(table_cols_keys[c])) {
                        rowBuffer.push('null');
                    } else {
                        rowBuffer.push('\'null\'');
                    }
                }
            }

            connection.query(query.fun_insert_into_update_on_duplicate_key(
                table_name,
                table_cols_keys,
                rowBuffer
            ), function (error, results, fields) {
                if (error) logger.lsql(error);
            });
            rowBuffer = [];

        } else {
            break;
        }
    }

    let totalRefreshTime = new Date().getTime() - startRefreshTime;
    logger.info('! SQL: \'' + table_local.table_name + '\' table referesh initialized from \'' + xls_path + '\' for '
        + totalRefreshTime / 1000
        + ' seconds');

    return true;
};
module.exports.refresh_db_data_from_xls = refreshDBDataFromXLS;