"use strict";

function drop_db_if_exists(db) {
    return "DROP DATABASE IF EXISTS " + db.db_name + ";";
};
module.exports.drop_db_if_exists = drop_db_if_exists;

let fun_create_db_if_not_exists = function (db) {
    return 'CREATE DATABASE IF NOT EXISTS ' + db.db_name + ';'
}
module.exports.fun_create_db_if_not_exists = fun_create_db_if_not_exists;

let fun_select_db = function (db) {
    return 'USE ' + db.db_name + ';';
}
module.exports.fun_select_db = fun_select_db;

let fun_drop_table_if_exists = function (table) {
    return 'DROP TABLE IF EXISTS `' + table.table_name + '`;';
}
module.exports.fun_drop_table_if_exists = fun_drop_table_if_exists;

let fun_create_table_if_not_exists = function (table_name, table_object,
    primary_keys,
    foreign_reference_table, foreign_keys, foreign_references,
    default_charset) {

    if (default_charset === undefined) {
        default_charset = 'utf8';
    }

    let result = 'CREATE TABLE IF NOT EXISTS `'
        + table_name + '` (';

    let colname_keys = Object.keys(table_object.cols);

    for (let k of colname_keys) {
        result += ('`' + table_object.cols[k].colname + '`'
            + (table_object.cols[k].coltype === undefined ? table_object.default_coltype : table_object.cols[k].coltype))
            + ','
    }
    result += 'PRIMARY KEY (';
    for (let i = 0; i < primary_keys.length; ++i) {
        result += primary_keys[i].colname + ',';
    }
    result = result.slice(0, -1);
    result += ')';

    if (foreign_reference_table && foreign_keys && foreign_references) {
        result += ',';
        for (let i = 0; i < foreign_keys.length; ++i) {
            result += "FOREIGN KEY (`" + foreign_keys[i].colname +
                '`) REFERENCES ' + foreign_reference_table.table_name + '(`' + foreign_references[i] + '`),';
        }
        result = result.slice(0, -1);
    }
    result += ') DEFAULT CHARSET=' + default_charset + ';';

    return result;
}
module.exports.fun_create_table_if_not_exists = fun_create_table_if_not_exists;

let row_vals = function (vals, brackets) {

    if (brackets === undefined) {
        brackets = true;
    }

    let result = (brackets ? '(' : '');
    for (let v of vals) {
        result += v + ',';
    }
    result = result.slice(0, -1);

    if (brackets) {
        result += ')';
    }

    return result;
};

let fun_insert_simple = function (table_name, cols, vals, semicolon = true) {
    let result = 'INSERT INTO '
        + table_name + ' '
        + row_vals(cols)
        + ' VALUES '
        + row_vals(vals) + (semicolon ? ';' : '');
    return result;
};
module.exports.fun_insert_simple = fun_insert_simple;

let fun_select_simple = function (table_name, cols, where, order_by, limit, semicolon = true, distinct = false) {

    let result = 'SELECT ' + (distinct ? " DISTINCT " : "")
        + row_vals(cols, false)
        + ' FROM ' + table_name;

    if (where && where != "") {
        result += ' WHERE ' + where;
    }

    if (order_by && order_by != "") {
        result += ' ORDER BY ' + order_by;
    }

    if (limit && limit != "") {
        result += ' LIMIT ' + limit;
    }

    result += (semicolon ? ';' : '');

    return result;
};
module.exports.fun_select_simple = fun_select_simple;

// RENAME TABLE old_table TO new_table;
let fun_rename_table = function (old_table, new_table) {
    return 'RENAME TABLE `' + old_table.table_name + '` TO `' + new_table.table_name + '`;';
}
module.exports.fun_rename_table = fun_rename_table;

// UPDATE table_name
// SET column1 = value1, column2 = value2, ...
// WHERE condition;
let fun_update_where = function (table_name, cols, vals, where_condition, semicolon = true) {

    let result = 'UPDATE ' + table_name
        + ' SET ';

    for (let i = 0; i < cols.length; ++i) {
        result += (cols[i] + ' = ' + vals[i] + ',');
    }
    result = result.slice(0, -1);

    result += (' WHERE (' + where_condition + ')' + (semicolon ? ';' : ''));

    return result;
}
module.exports.fun_update_where = fun_update_where;

let fun_insert_into_update_on_duplicate_key = function (table, cols, vals, semicolon = true) {

    let result = fun_insert_simple(table, cols, vals, false)
        + ' ON DUPLICATE KEY UPDATE ';

    for (let i = 0; i < cols.length; ++i) {
        result += (cols[i] + ' = ' + vals[i] + ',');
    }
    result = result.slice(0, -1);

    result += (semicolon ? ';' : '');

    return result;
}
module.exports.fun_insert_into_update_on_duplicate_key = fun_insert_into_update_on_duplicate_key;

let fun_create_user_and_grant = function (db, user) {
    return "GRANT " + row_vals(user.privileges, false)
        + " ON `" + db.db_name + '`.* TO \'' + user.name + '\'@\'' + user.host + '\' IDENTIFIED BY \'' + user.password + '\';';
}
module.exports.fun_create_user_and_grant = fun_create_user_and_grant;

function drop_user(user) {
    return "DROP USER IF EXISTS \'" + user.name + "\'@\'" + user.host + "\';";
}
module.exports.drop_user = drop_user;