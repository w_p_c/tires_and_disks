"use strict";

let path = require('path');

let database = require(path.join(__dirname + "/database/database"));

let router = require(path.join(__dirname + "/router/router"));