let form_upload_disks,
    form_upload_tires_gruz,
    form_upload_tires_legk;

let loading_upload_disks,
    loading_upload_tires_gruz,
    loading_upload_tires_legk;

let done_upload_disks,
    done_upload_tires_gruz,
    done_upload_tires_legk;

let wrong_upload_disks,
    wrong_upload_tires_gruz,
    wrong_upload_tires_legk;

let title_error_upload_disks,
    title_error_upload_tires_gruz,
    title_error_upload_tires_legk;

let title_done_upload_disks,
    title_done_upload_tires_gruz,
    title_done_upload_tires_legk;

$(document).ready(function () {
    form_upload_disks = document.getElementById("form-upload-disks");
    form_upload_tires_gruz = document.getElementById("form-upload-tires-gruz");
    form_upload_tires_legk = document.getElementById("form-upload-tires-legk");

    loading_upload_disks = document.getElementById("loading-upload-disks");
    loading_upload_tires_gruz = document.getElementById("loading-upload-tires-gruz");
    loading_upload_tires_legk = document.getElementById("loading-upload-tires-legk");

    done_upload_disks = document.getElementById("done-upload-disks");
    done_upload_tires_gruz = document.getElementById("done-upload-tires-gruz");
    done_upload_tires_legk = document.getElementById("done-upload-tires-legk");

    wrong_upload_disks = document.getElementById("wrong-upload-disks");
    wrong_upload_tires_gruz = document.getElementById("wrong-upload-tires-gruz");
    wrong_upload_tires_legk = document.getElementById("wrong-upload-tires-legk");

    title_error_upload_disks = document.getElementById("title-error-upload-disks");
    title_error_upload_tires_gruz = document.getElementById("title-error-upload-tires-gruz");
    title_error_upload_tires_legk = document.getElementById("title-error-upload-tires-legk");

    title_done_upload_disks = document.getElementById("title-done-upload-disks");
    title_done_upload_tires_gruz = document.getElementById("title-done-upload-tires-gruz");
    title_done_upload_tires_legk = document.getElementById("title-done-upload-tires-legk");
});

function onSubmitByNumber() {
    var action_url = "/admin/orders/number/" + document.querySelector("#form-find-by-number input").value;
    var form = document.getElementById("form-find-by-number");
    form.action = action_url;
};

function inputEmpty(id, valueMinInclusive = 0) {
    var value = document.getElementById(id).value;

    if (value == '' || value <= valueMinInclusive) {
        return false;
    } else {
        return true;
    }
};

function submit_upload() {
    // Request disks
    makeRequestUpload(
        form_upload_disks,
        loading_upload_disks,
        done_upload_disks,
        wrong_upload_disks,
        title_error_upload_disks,
        title_done_upload_disks
    );

    // Request tires-gruz
    makeRequestUpload(
        form_upload_tires_gruz,
        loading_upload_tires_gruz,
        done_upload_tires_gruz,
        wrong_upload_tires_gruz,
        title_error_upload_tires_gruz,
        title_done_upload_tires_gruz
    );

    // Request tires-legk
    makeRequestUpload(
        form_upload_tires_legk,
        loading_upload_tires_legk,
        done_upload_tires_legk,
        wrong_upload_tires_legk,
        title_error_upload_tires_legk,
        title_done_upload_tires_legk
    );
}

function makeRequestUpload(form, loading, img_done, img_wrong, title_error, title_done) {

    loading.style.display = "inline-block";

    let form_data = new FormData(form);

    // Request
    loading_upload_disks.style.display = "inline-block";
    $.ajax({
        url: "/admin/upload",
        data: form_data,
        type: 'POST',
        cache: false,
        contentType: false,
        processData: false
    }).done(function (data) {
        loading.style.display = "none";

        console.log(data);


        if (data == "done") {
            img_wrong.style.display = "none";
            title_error.style.display = "none";

            img_done.style.display = "inline-block";
            title_done.style.display = "inline";
            title_done.innerHTML = "- Файл загружен на сервер. Изменения вступят в силу через несколько минут."
        } else {
            img_wrong.style.display = "inline-block";
            title_error.style.display = "inline";

            if (data == "error-no-files") {
                title_error.innerHTML = "- Не выбран файл для загрузки";
            } else if (data == "error-auth") {
                title_error.innerHTML = "- Не аутентифицирован. Обновите страницу.";
            } else if (data == "error-format") {
                title_error.innerHTML = "- Загруженный файл имеет неправильный формат или повреждён.";
            }
        }
    });
}