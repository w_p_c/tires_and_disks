let basket_counter;

$(document).ready(function () {
    basket_counter = $('#basket-counter');
});

function on_image_error(element) {
    if (element.src != "/no_photo.png") {
        element.src = "/no_photo.png";
    }
}

function add_to_cart(idProduct, catalogue_type, element) {
    let href_query = "/basket/change";

    if (idProduct && catalogue_type) {
        href_query += "?id=" + idProduct + "&type=" + catalogue_type;
    }

    $.ajax({
        url: href_query,
        method: "POST"
    }).done(function (data) {
        if (data == "added") {
            element.classList.remove("buy-notadded");
            element.classList.add("buy-added");
            element.innerHTML = "В корзине";
            basket_counter.html(parseInt(basket_counter.html(), 10) + 1);
        } else if (data == "removed") {
            element.classList.remove("buy-added");
            element.classList.add("buy-notadded");
            element.innerHTML = "КУПИТЬ";
            basket_counter.html(parseInt(basket_counter.html(), 10) - 1);
        }
    });
}