$(document).ready(function () {
    $(".basikbox,.disc").click(function () {
        $('footer').fadeOut(0);
        $(".row").fadeOut(0);
        $(".button").fadeOut(0);
        $('.center-filter').fadeIn(1500).css('display', 'flex');
    });

    select_vendor_tires = $("#select-vendor-tires");
    select_model_tires = $("#select-model-tires");
    select_modif_tires = $("#select-modif-tires");
    select_year_tires = $("#select-year-tires");

    select_vendor_disks = $("#select-vendor-disks");
    select_model_disks = $("#select-model-disks");
    select_modif_disks = $("#select-modif-disks");
    select_year_disks = $("#select-year-disks");

    if (select_vendor_tires.val() != null) {
        on_change_select_vendor(select_vendor_tires.val(), "tires");
    }

    if (select_vendor_disks.val() != null) {
        on_change_select_vendor(select_vendor_disks.val(), "disks");
    }
});

function on_change_select_vendor(selected_value, type) {
    let select_model;

    if (type == "tires") {
        select_model = $("#select-model-tires");
    } else if (type == "disks") {
        select_model = $("#select-model-disks");
    }

    if (selected_value) {
        $.ajax({
            url: "/main/select?vendor=" + selected_value,
            method: "POST"
        }).done(function (data) {
            console.log(data);

            select_model.html("");
            for (val of data) {
                select_model.append('<option value="' + val.car + '" selected>' + val.car + '</option>');
            }

            if (select_model.val() != null) {
                on_change_select_model(select_model.val(), type);
            }
        });
    }
}

function on_change_select_model(selected_value, type) {

    let select_vendor, select_modif;

    if (type == "tires") {
        select_vendor = $("#select-vendor-tires");
        select_modif = $("#select-modif-tires");
    } else if (type == "disks") {
        select_vendor = $("#select-vendor-disks");
        select_modif = $("#select-modif-disks");
    }

    if (select_vendor.val() != null && selected_value) {
        $.ajax({
            url: "/main/select?vendor=" + select_vendor.val() + "&model=" + selected_value,
            method: "POST"
        }).done(function (data) {
            console.log(data);

            select_modif.html("");
            for (val of data) {
                select_modif.append('<option value="' + val.modification + '" selected>' + val.modification + '</option>');
            }

            if (select_modif.val() != null) {
                on_change_select_modif(select_modif.val(), type);
            }
        });
    }
}

function on_change_select_modif(selected_value, type) {

    let select_vendor, select_modif, select_year, select_model;

    if (type == "tires") {
        select_model = $("#select-model-tires");
        select_vendor = $("#select-vendor-tires");
        select_modif = $("#select-modif-tires");
        select_year = $("#select-year-tires");
    } else if (type == "disks") {
        select_model = $("#select-model-disks");
        select_vendor = $("#select-vendor-disks");
        select_modif = $("#select-modif-disks");
        select_year = $("#select-year-disks");
    }

    if (select_vendor.val() != null && select_modif.val() != null && selected_value) {
        $.ajax({
            url: "/main/select?vendor=" + select_vendor.val() + "&model=" + select_model.val() + "&modif=" + selected_value,
            method: "POST"
        }).done(function (data) {
            console.log(data);

            select_year.html("");
            for (val of data) {
                select_year.append('<option value="' + val.year + '" selected>' + val.year + '</option>');
            }
        });
    }
}

function click_find_by_auto(type) {

    let select_vendor, select_modif, select_year, select_model;

    if (type == "tires") {
        select_model = $("#select-model-tires");
        select_vendor = $("#select-vendor-tires");
        select_modif = $("#select-modif-tires");
        select_year = $("#select-year-tires");
    } else if (type == "disks") {
        select_model = $("#select-model-disks");
        select_vendor = $("#select-vendor-disks");
        select_modif = $("#select-modif-disks");
        select_year = $("#select-year-disks");
    }

    if (select_vendor.val() != null
        && select_model.val() != null
        && select_modif.val() != null
        && select_year.val() != null) {

        window.location.replace('/auto?vendor=' + select_vendor.val()
            + "&model=" + select_model.val()
            + "&modif=" + select_modif.val()
            + "&year=" + select_year.val());
    }
}