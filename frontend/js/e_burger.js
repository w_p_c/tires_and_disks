let menu;

$(document).ready(function () {
    menu = document.querySelector(".menu");

    $(".menu-wrapper").on("click", function () {
        $(".hamburger-menu").toggleClass("animate");
        menu.classList.toggle("active-menu");
    });
});