let select_model;
let select_brand;
let item_container;
let basket_counter;
let button_search_form;

$(document).ready(function () {

    var parent = document.querySelector(".range-slider");
    if (!parent) return;

    var
        rangeS = parent.querySelectorAll("input[type=range]"),
        numberS = parent.querySelectorAll("input[type=number]");

    rangeS.forEach(function (el) {
        el.oninput = function () {
            var slide1 = parseFloat(rangeS[0].value),
                slide2 = parseFloat(rangeS[1].value);

            if (slide1 > slide2) {
                [slide1, slide2] = [slide2, slide1];

            }

            numberS[0].value = slide1;
            numberS[1].value = slide2;
        }
    });

    numberS.forEach(function (el) {
        el.oninput = function () {
            var number1 = parseFloat(numberS[0].value),
                number2 = parseFloat(numberS[1].value);

            if (number1 > number2) {
                var tmp = number1;
                numberS[0].value = number2;
                numberS[1].value = tmp;
            }

            rangeS[0].value = number1;
            rangeS[1].value = number2;

        }
    });

    var $rangePrice = $("#rangePrice");
    $rangePrice
        .on("slide", function (event, ui) {
            $("#amountPrice").text(
                "$" + $rangePrice.slider("values", 0) +
                " - " +
                "$" + $rangePrice.slider("values", 1)
            );
        });

    // Init models list
    select_model = $('#select-ajax-model');
    select_brand = $('#select-ajax-brand');
    item_container = $('#items-ajax-container');

    on_change_select_brand(select_brand.val(), (document.querySelector("#catalogue-type-disks-indicator") ? "disks" : "tires"));

    basket_counter = $('#basket-counter');

    button_search_form = document.getElementById("button-search-form");
});

const regexp_limit_url = /^.+(limit=([0-9]+)).*$/;
function click_load_more(catalogue_type) {
    let old_limit = 25;

    let regexp_match_empty_query = new RegExp("^.+" + catalogue_type + "$");
    let href_item = "/" + catalogue_type;

    // Query limit
    let href_query = document.URL;
    let query_match = href_query.match(regexp_limit_url);
    if (query_match) {
        old_limit = parseInt(query_match[2], 10);
        href_query = href_query.replace(query_match[1], "limit=" + (parseInt(query_match[2], 10) + 25));
    } else if (regexp_match_empty_query.test(href_query)) {
        href_query += "?limit=50"
    } else {
        href_query += "&limit=50";
    }
    href_query += "&ajax_load_more=true";

    $.ajax({
        url: href_query,
        method: "GET"
    }).done(function (data) {
        history.pushState(null, null, href_query.replace("&ajax_load_more=true", ""));

        console.log(data);

        let items = data[0], basket_id = data[1], already_in_basket;
        for (let i = old_limit; i < items.length; ++i) {
            already_in_basket = basket_id.includes(items[i].idProduct.toString());
            item_container.append('<div class="item"><a href="' + href_item + "/" + items[i].idProduct + '">'
                + '<div class="img-item"><img src="/' + (catalogue_type == "disks" ? items[i].photo_1 : items[i].photo) + '" onerror="on_image_error(this)"/></div></a>'
                + '<div class="line-item"></div><a href="' + href_item + "/" + items[i].idProduct + '">'
                + '<p class="text-item">' + items[i].name + '</p></a>'
                + '<div class="row-img-button-price">'
                + '<p class="text-disc-item">' + (catalogue_type == "disks" ? items[i].wheel_type : items[i].season) + '</p>'
                + '<div class="text-center-buy"><span>' + Math.ceil(items[i].end_rozn_price) + '</span><span>&nbspгрн</span></div>'
                + '<button class="buy' + (already_in_basket ? " buy-added" : " buy-notadded") + '" onclick="add_to_cart(\'' + items[i].idProduct + '\',\'' + catalogue_type + '\', this)">'
                + (already_in_basket ? "В корзине" : "КУПИТЬ") + '</button>'
                + '</div></div></div>');
        }
    });

    /*
    <form class="item"><a href="/tires">
        <div class="img-item"><img src="/disc-item.png" alt=""/></div></a>
      <div class="line-item"><a href="/tires">
          <p class="text-item">item.name</p></a>
        <div class="row-img-button-price">
          <p class="text-disc-item">item.wheel_type</p>
          <div class="text-center-buy"><span>Math.ceil(item.end_rozn_price) </span><span>&nbspгрн</span></div>
          <button class="buy">КУПИТЬ</button>
        </div>
      </div>
    </form>
    */
}

const regexp_model_url = /^.+model=([^&]+).*$/;
function on_change_select_brand(selected_value, catalogue_type) {

    if (catalogue_type) {
        $.ajax({
            url: "/" + catalogue_type + "/select?brand=" + selected_value,
            method: "POST"
        }).done(function (data) {
            let doc_url_match = decodeURIComponent(document.URL).match(regexp_model_url);

            if (doc_url_match) {
                if (doc_url_match[1] == "all") {
                    select_model.html('<option value="all" selected>Модель</option>');
                } else {
                    select_model.html('<option value="all">Модель</option>');
                }

                for (val of data) {
                    select_model.append('<option value="' + val.model
                        + '"' + (val.model == doc_url_match[1].replace("+", " ")    ? " selected" : "") + '>'
                        + val.model + '</option>');
                }
            } else {
                select_model.html('<option value="all" selected>Модель</option>');
                for (val of data) {
                    select_model.append('<option value="' + val.model + '">' + val.model + '</option>');
                }
            }
        });
    } else {
        select_model.html('<option value="all" selected>Модель</option>');
    }
}

function on_image_error(element) {
    if (element.src != "/no_photo.png") {
        element.src = "/no_photo.png";
    }
}

function add_to_cart(idProduct, catalogue_type, element) {
    let href_query = "/basket/change";

    if (idProduct && catalogue_type) {
        href_query += "?id=" + idProduct + "&type=" + catalogue_type;
    }

    $.ajax({
        url: href_query,
        method: "POST"
    }).done(function (data) {
        if (data == "added") {
            element.classList.remove("buy-notadded");
            element.classList.add("buy-added");
            element.innerHTML = "В корзине";
            basket_counter.html(parseInt(basket_counter.html(), 10) + 1);
        } else if (data == "removed") {
            element.classList.remove("buy-added");
            element.classList.add("buy-notadded");
            element.innerHTML = "КУПИТЬ";
            basket_counter.html(parseInt(basket_counter.html(), 10) - 1);
        }
    });
}

function on_change_order_by() {
    button_search_form.click();
}