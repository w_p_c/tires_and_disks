let total_sum;
let basket_counter;
$(document).ready(function () {
    total_sum = document.getElementById("total-sum");
    basket_counter = document.getElementById("basket-counter");
});

function validate_personal_data() {
    let result = true;

    //Считаем значения из полей name и email в переменные x и y
    var x = document.forms['form_basket']['name'].value;
    var y = document.forms['form_basket']['email'].value;
    var z = document.forms['form_basket']['phone'].value;
    //Если поле name пустое выведем сообщение и предотвратим отправку формы
    if (x.length == 0) {
        document.getElementById('namef').innerHTML = '*данное поле обязательно для заполнения';
        result = false;
    }
    //Если поле email пустое выведем сообщение и предотвратим отправку формы
    if (y.length == 0) {
        document.getElementById('emailf').innerHTML = '*данное поле обязательно для заполнения';
        result = false;
    }
    if (z.length == 0) {
        document.getElementById('numberid').innerHTML = '*данное поле обязательно для заполнения';
        result = false;
    }
    if (x.length > 0 && y.length > 0 && z.length > 0) {
        $(".center-order-two").fadeOut(0);
        $('.center-order').fadeIn(1500).css('display', 'flex').css('margin', '0 auto').css('flex-direction', 'column').css('max-width', '375px');
        result = false;
    }

    return result;
}

// Check if all radios is checked
function validate_payment_and_shipping() {
    let result = true;

    if (document.forms['form_basket']['shipping_address_country'].value.length == 0) {
        document.getElementById('shipping_address_country_f').innerHTML = '*данное поле обязательно для заполнения';
        result = false;
    }

    if (!$("input[name='shipping_type']:checked").val()) {
        document.getElementById('shipping_type_f').innerHTML = '*данное поле обязательно для заполнения';
        result = false;
    }

    if (document.forms['form_basket']['shipping_address_department'].value.length == 0) {
        document.getElementById('shipping_address_department_f').innerHTML = '*данное поле обязательно для заполнения';
        result = false;
    }

    if (!$("input[name='payment_type']:checked").val()) {
        document.getElementById('payment_type_f').innerHTML = '*данное поле обязательно для заполнения';
        result = false;
    }

    return result;
}

function submit_form() {
    if (validate_payment_and_shipping()) {
        if (parseInt(total_sum.innerHTML, 10) <= 0) {
            document.getElementById("rm-2").style.display = "none";
            document.getElementById("empty-cart-half").style.display = "inline";
        } else {
            let form = document.forms['form_basket'];

            let arr_id_amount_price = [];
            for (el of document.querySelectorAll(".data-holder")) {
                arr_id_amount_price.push({
                    id: el.getAttribute("idproduct"),
                    amount: el.getAttribute("amount"),
                    price_moment: el.parentElement.parentElement.querySelector(".data-holder-price").innerHTML,
                    name: el.innerHTML,
                    type: el.getAttribute("type")
                });
            }

            let href_submit = "/basket/submit?"
                + "&name=" + form["name"].value
                + "&email=" + form["email"].value
                + "&phone=" + form["phone"].value
                + "&shipping_address_country=" + form['shipping_address_country'].value
                + "&shipping_type=" + form["shipping_type"].value
                + "&shipping_address_department=" + form['shipping_address_department'].value
                + "&payment_type=" + form["payment_type"].value
                + "&order_positions=" + JSON.stringify(arr_id_amount_price)
                + "&total_sum=" + document.getElementById("total-sum").innerHTML;
            $.ajax({
                url: href_submit,
                method: "POST"
            }).done(function (data) {

                document.getElementById("rm-1").style.display = "none";
                document.getElementById("rm-2").style.display = "none";

                if (data == "submitted") {
                    document.getElementById("success-bg").style.display = "flex";
                } else {
                    document.getElementById("error-bg").style.display = "flex";
                }
            });
        }
    } else {
        return;
    }
}

function on_image_error(element) {
    if (element.src != "/no_photo.png") {
        element.src = "/no_photo.png";
    }
}

function remove_item_basket(cross) {
    cross.setAttribute("deleteitemclick", "clicked");
}

function on_item_form_click(form, idProduct, type, item_price) {

    let plus_minus_input = form.querySelector(".plus-minus");

    if (form.querySelector(".remove-cross").getAttribute("deleteitemclick") == "clicked") {
        let href_query = "/basket/change";

        if (idProduct && type) {
            href_query += "?id=" + idProduct + "&type=" + type;
        }

        $.ajax({
            url: href_query,
            method: "POST"
        }).done(function (data) {
            if (data == "removed") {
                form.remove();
                total_sum.innerHTML = parseInt(total_sum.innerHTML, 10) - (parseInt(plus_minus_input.value, 10) * parseInt(item_price, 10));
                basket_counter.innerHTML = parseInt(basket_counter.innerHTML, 10) - 1;
            }
        });
    }
}

function change_item_amount(action, item_price, element, item) {

    let plus_minus_input = element.parentElement.querySelector(".plus-minus");
    let data_holder = element.parentElement.querySelector(".data-holder");

    let total_sum_int = parseInt(total_sum.innerHTML, 10);
    let result_price, result_input;
    if (action == "+") {
        result_input = parseInt(plus_minus_input.value, 10) + 1;
        result_price = total_sum_int + parseInt(item_price, 10);
    } else if (action == "-") {
        result_input = parseInt(plus_minus_input.value, 10) - 1;
        result_price = total_sum_int - parseInt(item_price, 10);
    }

    if (result_input < 1) {
        result_input = 1;
        result_price = total_sum_int;
    }

    total_sum.innerHTML = result_price;

    plus_minus_input.value = result_input;
    data_holder.setAttribute("amount", result_input);
}